from django.db import models

# Create your models here.
class Autor(models.Model):
    nombre = models.CharField(max_length=200, blank=True)
    apellido = models.CharField(max_length=200, blank=True)
    pseudonimo = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.nombre + ' ' + self.apellido + ' ' + self.pseudonimo

class GeneroLiterario(models.Model):
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Editorial(models.Model):
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Libro(models.Model):
    titulo = models.CharField(max_length=200)
    autores = models.ManyToManyField(Autor)
    genero = models.ForeignKey(to=GeneroLiterario, on_delete=models.SET_NULL, null=True)
    editorial = models.ForeignKey(to=Editorial, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.titulo + ' ' + self.editorial.nombre + ' ' + self.genero.nombre

class Ejemplar(models.Model):
    disponible = models.IntegerField()
    libro = models.ForeignKey(to=Libro, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.disponible + ' ' + self.libro.titulo

class Usuario(models.Model):
    Clave = models.DateField()
    Nombre = models.CharField(max_length=200)
    Apellido = models.CharField(max_length=200)
    Correo = models.EmailField()
    Telefono =  models.IntegerField()
    Direccion = models.CharField(max_length=200)
    def __str__(self):
        return self.Nombre + ' ' + self.Apellido + ' ' + self.Correo + ' ' + self.Telefono + ' ' + self.Direccion

class Prestamo(models.Model):
    Fecha_devolucion = models.DateField(blank=True)
    Fecha_entrega = models.DateField(blank=True)
    Limite_entrega = models.DateField()
    Limite_devolucion = models.DateField(blank=True)
    ejemplar = models.ForeignKey(to=Ejemplar, on_delete=models.SET_NULL, null=True)
    usuario = models.ForeignKey(to=Usuario, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.Fecha_devolucion + ' ' + self.Fecha_entrega + ' ' + self.Limite_entrega + ' ' + self.Limite_devolucion
