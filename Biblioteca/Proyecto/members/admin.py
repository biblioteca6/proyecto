from django.contrib import admin

# Register your models here.
from .models import Autor, Libro, GeneroLiterario, Editorial, Ejemplar, Prestamo, Usuario
from django.contrib import admin
# Register your models here.
admin.site.register(Autor)
admin.site.register(Libro)
admin.site.register(GeneroLiterario)
admin.site.register(Editorial)
admin.site.register(Ejemplar)
admin.site.register(Prestamo)
admin.site.register(Usuario)
