from django.db import models

# Create your models here.
class Libro(models.Model):
    titulo = models.CharField(maxlength=200)
    autores = models.ManyToManyField(Autor)

class Autor(models.Model):
    nombre = models.CharField(maxlength=200)
    apellido = models.CharField(maxlength=200)
    pseudonimo = models.CharField(maxlength=200)

class GeneroLiterario(models.Model):
    nombre = models.CharField(maxlength=200)

class Editorial(models.Model):
    nombre = models.CharField(maxlength=200)

class Ejemplar(models.Model):
    disponible = models.IntField(maxlength=200)

class Prestamo(models.Model):
    Fecha_devolucion = models.DateField()
    Fecha_entrega = models.DateField()
    Limite_entrega = models.DateField()
    Limite_devolucion = models.DateField()

class Usuario(models.Model):
    Clave = models.DateField()
    Nombre = models.CharField(maxlength=200)
    Apellido = models.CharField(maxlength=200)
    Correo = models.EmailField()
    Telefono =  models.IntField()
    Direccion = models.CharField(maxlength=200)
